package backend.hangman.controller;

import backend.hangman.model.GameResult;
import backend.hangman.repo.GameResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hangman")
public class GameResultController {

    @Autowired
    private GameResultRepository gameResultRepository;

    @PostMapping("/save")
    public ResponseEntity<GameResult> saveGameResult(@RequestBody GameResult gameResult) {
        return new ResponseEntity<>(gameResultRepository.save(gameResult), HttpStatus.CREATED);
    }

    @GetMapping("/results")
    public ResponseEntity<Iterable<GameResult>> getAllResults() {
        return new ResponseEntity<>(gameResultRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/test")
    public ResponseEntity<Object> getTest() {
        return new ResponseEntity<>("Hi, test works!", HttpStatus.OK);
    }
}
