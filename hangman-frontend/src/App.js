import './App.css';
import Navbar from './components/navbar/Navbar';
import HangmanPage from './pages/HangmanPage';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';

function App() {
  return (
    <div className="App">
      <Navbar />
      {/* <HomePage /> */}
      <HangmanPage />
      <LoginPage /> 
    </div>
  );
}

export default App;
