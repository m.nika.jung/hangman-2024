import React from "react";
import "./Button.css";

const Button = (props) => {
  return (
    <button
      id={props.id}
      className={`button ${props.buttonType}`}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

export default Button;
