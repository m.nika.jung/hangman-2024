import React from "react";
import "./Category.css";

const Category = (props) => {
  return (
    <div className="category-area">
      <div className="cat-label">CATEGORY</div>
      <div className="cat-content">{props.category}</div>
    </div>
  );
};

export default Category;
