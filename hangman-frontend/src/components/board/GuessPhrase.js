import React, { useRef } from "react";
import Button from "../Button";
import Category from "./Category";
import PhraseInputs from "./PhraseInputs";
import "./GuessPhrase.css";

const GuessPhraseArea = ({ category, guessedPhrase, onSubmit }) => {
  const inputsRef = useRef([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validInputs()) {
      const fullPhrase = guessedPhrase
        .split("")
        .map((char, index) =>
          inputsRef.current[index] && inputsRef.current[index].value
            ? inputsRef.current[index].value.toUpperCase()
            : char
        )
        .join("");
      onSubmit(fullPhrase);
      clearInputs();
    } else {
      console.log("Invalid inputs");
    }
  };

  const validInputs = () =>
    inputsRef.current.every(
      (input) =>
        input === undefined ||
        input === null ||
        (input && input.value.match(/^[a-zA-Z]$/))
    );

  const clearInputs = () => {
    inputsRef.current.forEach((input) => {
      if (input) {
        input.value = "";
      }
    });
  };

  const onKeyUpHandler = (event, index) => {
    if (
      event.key.match(/^[a-zA-Z]$/) &&
      event.key.length > 0 &&
      index + 1 < inputsRef.current.length &&
      inputsRef.current[index + 1] !== null &&
      inputsRef.current[index + 1] !== undefined
    ) {
      inputsRef.current[index + 1].focus();
    } else {
      let i = index + 1;
      while (i + 1 < inputsRef.current.length) {
        if (
          event.key.match(/^[a-zA-Z]$/) &&
          event.key.length > 0 &&
          inputsRef.current[i + 1] !== null &&
          inputsRef.current[i + 1] !== undefined
        ) {
          inputsRef.current[i + 1].focus();
          break;
        }
        i = i + 1;
      }
    }
  };

  return (
    <form className="phase-form" onSubmit={handleSubmit}>
      <div className="phase-content">
        <PhraseInputs
          guessedPhrase={guessedPhrase}
          inputsRef={inputsRef}
          onKeyUpHandler={onKeyUpHandler}
        />
      </div>

      <Category category={category} />
      <Button buttonType="guess primary" id="guess-button">
        Guess whole phrase!
      </Button>
    </form>
  );
};

export default GuessPhraseArea;
