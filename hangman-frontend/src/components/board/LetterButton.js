import React from "react";
import Button from "../Button";

const LetterButton = (props) => {
  var disabled = "";
  var notClickable = "";
  if (props.isDisabled) disabled = "disabled";
  if (props.isNotClickable) notClickable = "not-clickable";
  return (
    <Button
      id={props.id}
      buttonType={`secondary letter ${disabled} ${notClickable}`}
      onClick={props.onClick}
    >
      {props.children}
    </Button>
  );
};

export default LetterButton;
