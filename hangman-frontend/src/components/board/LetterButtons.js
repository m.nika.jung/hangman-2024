import React, { useState } from "react";
import LetterButton from "./LetterButton";
import "./LetterButtons.css";

const LetterButtons = (props) => {
  const [disabledLetters, setDisabledLetters] = useState([]);

  const handleLetterClick = (letter) => {
    setDisabledLetters([...disabledLetters, letter]);
    props.onLetterClick(letter);
  };


  return (
    <div className="letter-buttons">
      {props.letters.map((letter, index) => (
        <LetterButton
          key={`letter-button-${letter}`}
          id={`letter-button-${letter}`}
          isDisabled={disabledLetters.includes(letter)}
          isNotClickable={props.isGameOver}
          onClick={!props.isGameOver ? (() => handleLetterClick(letter)) : (()=>{})}
        >
          {letter}
        </LetterButton>
      ))}
    </div>
  );
};

export default LetterButtons;
