import React from 'react';

const PhraseInputs = ({ guessedPhrase, inputsRef, onKeyUpHandler }) => {
  return (
    <>
      {guessedPhrase.split("").map((char, index) => {
        if (char === "_") {
          return (
            <input
              key={`ph-input-${index}`}
              ref={(el) => inputsRef.current[index] = el}
              className="input-char"
              type="text"
              required
              pattern="[A-Za-z]"
              placeholder={char}
              maxLength={1}
              autoComplete="off"
              onKeyUp={(e) => onKeyUpHandler(e, index)}
            />
          );
        } else if (char === " ") {
          return <span key={`ph-space-${index}`} className="space">{" "}</span>;
        } else {
          return <span key={`ph-char-${index}`} className="guessed-char">{char}</span>;
        }
      })}
    </>
  );
};

export default PhraseInputs;
