import React, { useState } from "react";
import Button from "../Button";
import "./LoginForm.css";
import Variables from "../variables";

function LoginForm({ isSignUp, onSubmit }) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [usernameMsg, setUsernameMsg] = useState("");
  const [emailMsg, setEmailMsg] = useState("");
  const [passwordMsg, setPasswordMsg] = useState("");
  const [password2Msg, setPassword2Msg] = useState("");
  const [showValidInfo, setShowValidInfo] = useState(false);

  function onUsernameChange(event) {
    setUsername(event.target.value);
  }
  function onEmailChange(event) {
    setEmail(event.target.value);
  }
  function onPasswordChange(event) {
    setPassword(event.target.value);
  }
  function onPassword2Change(event) {
    setPassword2(event.target.value);
  }

  function validateFields() {
    let msg = "";
    if (isSignUp) {
      if (username.length < 5)
        msg = msg + "The username must have at least 5 letters.";
    }
  }

  function validateText(
    text,
    fieldname,
    reqLength,
    reqBigLetter,
    reqNumber,
    reqSpecialChar
  ) {
    const specialChars = ["!", "@", "#", "$", "%", "&", "*", "-", "_"];
    let msg = "";
    if (text.length < reqLength)
      msg = msg + `${fieldname} must have at least ${reqLength} chars.`;
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (validateFields()) onSubmit();
    else setShowValidInfo(true);
  }

  return (
    <form className="login-form" onSubmit={handleSubmit}>
      <div className="inputs-area">
        {isSignUp && (
          <>
            <input
              type="text"
              id="username"
              placeholder="Username"
              value={username}
              onChange={onUsernameChange}
              className="form-input"
              required
            />
            <p>{usernameMsg}</p>
          </>
        )}
        <input
          type="email"
          id="email"
          placeholder="Email"
          value={email}
          onChange={onEmailChange}
          required
        />
        <p>{emailMsg}</p>
        <input
          type="password"
          id="password"
          placeholder="Password"
          value={password}
          onChange={onPasswordChange}
          required
        />
        <p>{passwordMsg}</p>
        {isSignUp && (
          <>
            <input
              type="password"
              id="password2"
              placeholder="Repeat password"
              value={password2}
              onChange={onPassword2Change}
              required
            />
            <p>{password2Msg}</p>
          </>
        )}
      </div>
      <Button type="submit" buttonType="primary form">
        {isSignUp ? "Sign Up" : "Log In"}
      </Button>
    </form>
  );
}

export default LoginForm;
