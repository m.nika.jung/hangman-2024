import React from "react";
import "./PhotoCarousel";

function PhotoCarousel() {
  return (
    <div className="carousel">
      <img
        className="home-page-img"
        src="/images/home-page-library.jpg"
        alt="Home Page Library"
      />
    </div>
  );
}

export default PhotoCarousel;
