import React from "react";
import "./Navbar.css";

const Navbar = () => {
  return (
    <ul className="navbar">
        <li className="navOption active" key="nav-home">Home</li>
        <li className="navOption" key="nav-rules">Rules</li>
        <li className="navOption" key="nav-statistics">Statisctics</li>
        <li className="navOption" key="nav-recommendations">Recommendations</li>
        <li className="navOption last" key="nav-about">About Us</li>
        <li className="navOption user last" key="nav-user">User</li>
    </ul>
  );
};

export default Navbar;
