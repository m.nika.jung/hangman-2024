import React from 'react';
import './PopUp.css';  // Importowanie stylów CSS
import Button from '../Button';

const PopUp = ({ title, description, buttons }) => {

  return (
    <div className="popup-overlay">
      <div className="popup">
        <h2 className="popup-title">{title}</h2>
        <p className="popup-description">{description}</p>
        <div className="popup-buttons">
          {buttons.map((button, index) => (
            <Button key={`popup-button-${index}`} onClick={button.onClick} buttonType={`popup ${button.buttonType}`}>
              {button.label}
            </Button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PopUp;
