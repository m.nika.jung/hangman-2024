const Variables = {
    englishLetters: ["A", "B", "C", "D", "E", "F", "H",
        "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z"],
    englishLettersSmall: ["a", "b", "c", "d", "e", "g", "h",
        "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z"],
    polishLetters: ["A", "Ą", "B", "C", "Ć", "D", "E",
        "Ę", "F", "H", "I", "J", "K", "L", "Ł", "M",
        "N", "Ń", "O", "Ó", "P", "Q", "R", "S", "Ś",
        "T", "U", "W", "Y", "Z", "Ź", "Ż"],
    allLetters: ["A", "B", "C", "D", "E", "F", "H",
        "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z", "Ą", "Ć",
        "Ę", "Ł", "Ń", "Ó", "Ś", "Ź", "Ż"],
}

export default Variables;
