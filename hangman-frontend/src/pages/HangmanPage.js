import React, { useState } from "react";
import "./HangmanPage.css";
import Variables from "../components/variables";
import GuessPhrase from "../components/board/GuessPhrase";
import LetterButtons from "../components/board/LetterButtons";
import PopUp from "../components/notify/PopUp";

const HangmanPage = (props) => {
  const fullPhrase = "ala ma dwa kotki".toUpperCase();
  const category = "Animal crossing".toUpperCase();
  const maxTries = 12;
  const [guessedLetters, setGuessedLetters] = useState([]);
  const [tries, setTries] = useState(maxTries);
  const [points, setPoints] = useState(0);
  const [gameState, setGameState] = useState({ isOver: false, isWon: false });
  const [isPopUpVisable, setIsPopUpVisable] = useState(false);
  const [isGameResultSent, setIsGameResultSent] = useState(false);

  const postGameResult = async () => {
    const gameResult = {
      playerId: "1",
      category: category,
      phase: fullPhrase,
      score: points,
      leftTries: tries,
      isWin: gameState.isWon
    };

    try {
      const response = await fetch('http://localhost:8080/hangman/save', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(gameResult)
      });
      if (response.ok) {
        setIsGameResultSent(true);
      }
      const data = await response.json();
      console.log('Game result posted:', data);
    } catch (error) {
      console.error('Error posting game result:', error);
    }
  };

  const handleLetterClick = (letter) => {
    setGuessedLetters([...guessedLetters, letter.toUpperCase()]);
    const letters = [...guessedLetters, letter.toUpperCase()];
    const guessed = generateDisplayedPhrase(letters);
    if (guessed === fullPhrase) {
      console.log(guessed);
      console.log("Correct guess!");
      setGameState({ isOver: true, isWon: true });
      setIsPopUpVisable(true);
      if (!isGameResultSent) postGameResult();
    } else {
      if (!fullPhrase.includes(letter)) handleGuessTry();
    }
  };

  const handlePhaseSubmit = (guessedPhrase) => {
    if (guessedPhrase === fullPhrase) {
      console.log("Correct guess!");
      setGameState({ isOver: true, isWon: true });
      setIsPopUpVisable(true);
      if (!isGameResultSent) postGameResult();
    } else {
      console.log("Incorrect guess.");
      handleGuessTry();
    }
  };

  function handleGuessTry() {
    if (tries > 0) {
      setTries(tries - 1);
      setPoints(points - 1);
    } else {
      console.log("Game over!");
      setGameState({ isOver: true, isWon: false });
      setIsPopUpVisable(true);
      if (!isGameResultSent) postGameResult();
    }
  }

  const generateDisplayedPhrase = (guessedLetters) => {
    return fullPhrase
      .split("")
      .map((char) => {
        return guessedLetters.includes(char.toUpperCase())
          ? char
          : char === " "
          ? " "
          : "_";
      })
      .join("");
  };

  const handleClosePopUp = () => {
    setIsPopUpVisable(false);
  };

  const handleStartNewGame = () => {
    console.log("New game should be started");
  };

  const popupButtons = [
    {
      label: "New game",
      onClick: () => handleStartNewGame,
      buttonType: "",
    },
    { label: "Close", onClick: handleClosePopUp, buttonType: "" },
  ];

  return (
    <div className="hangman-page">
      {gameState.isOver && isPopUpVisable && (
        <PopUp
          onClose={handleClosePopUp}
          title={gameState.isWon ? "You won!" : "Maybe next time"}
          description={
            gameState.isWon
              ? `You guessed the phase correct and collected ${points} points.`
              : `You did not guess the phase correctly. Your points: ${points}.`
          }
          buttons={popupButtons}
        />
      )}
      <div className="letter-area">
        <LetterButtons
          letters={Variables.englishLetters}
          onLetterClick={handleLetterClick}
          isGameOver={gameState.isOver}
        />
      </div>

      <div className="guess-phase-area">
        <GuessPhrase
          category={category}
          guessedPhrase={
            gameState.isWon
              ? fullPhrase
              : generateDisplayedPhrase(guessedLetters)
          }
          onSubmit={handlePhaseSubmit}
          allowedLetters={props.letters}
        />
      </div>

      <div className="img-area">
        <h3>Left tries: {tries}</h3>
        <img
          className="hangman-img"
          src={`/images/hangman-${12 - tries}.jpg`}
          alt="hangman"
        />
        <h3>Your score: {points}</h3>
      </div>
    </div>
  );
};

export default HangmanPage;
