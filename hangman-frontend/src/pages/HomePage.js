import React from "react";
import "./HomePage.css";
import Button from "../components/Button";

const HomePage = () => {
  return (
    <div id="home-page" className="home-page">
      <div className="home-page-content">
        <h1 className="title">Welcome!</h1>
        <div className="menu">
          <Button buttonType="primary">Log in or register</Button>
          <Button buttonType="secondary">Play as a guest</Button>
        </div>
      </div>
      <img
        className="home-page-img"
        src="/images/home-page-library.jpg"
        alt="Home Page Library"
      />
    </div>
  );
};

export default HomePage;
