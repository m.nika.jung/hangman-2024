import React, { useState } from 'react';
import LoginForm from "../components/login/LoginForm";
import PhotoCarousel from "../components/login/PhotoCarousel";
import './LoginPage.css';
import Button from '../components/Button';

function LoginPage() {
    const [isSignUp, setIsSignUp] = useState(false);
    
    const onLoginSubmit = () => {

    }

    const onSignUpSubmit = () => {

    }

    return (
        <div className="login-page">
            <div className="photo-carousel-area">
                <PhotoCarousel />
            </div>
            <div className="content-area">
                <div className="form-title">
                    <h1>{isSignUp ? 'Sign Up' : 'Log In'}</h1>
                </div>
                <div className="form-area">
                   <LoginForm onSubmit={onLoginSubmit} isSignUp={isSignUp}/>
                </div>
                <button className="form-switch-button" onClick={() => setIsSignUp(!isSignUp)}>
                    {isSignUp ? 'Already have an account? Log In' : 'Need an account? Sign Up'}
                </button>
                <Button buttonType="secondary form second">
                    Sign in with Google
                </Button>
            </div>
        </div>
    );
}

export default LoginPage;
