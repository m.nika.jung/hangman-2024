#!/bin/bash

echo "Starting Hangman backend..."
cd hangman-backend
mvn spring-boot:run &
BACKEND_PID=$!

echo "Starting Hangman frontend..."
cd ../hangman-frontend
npm start &
FRONTEND_PID=$!

# Wait for any process to exit
wait $BACKEND_PID $FRONTEND_PID

# If any process exits, kill the other
kill $BACKEND_PID
kill $FRONTEND_PID