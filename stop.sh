#!/bin/bash

# Znajdź i zakończ proces Spring Boot (backend)
echo "Stopping Hangman backend..."
pkill -f 'java.*hangman-backend'

# Znajdź i zakończ proces npm (frontend)
echo "Stopping Hangman frontend..."
pkill -f 'node.*hangman-frontend'

echo "Processes have been stopped."
